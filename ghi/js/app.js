function createCard(name, description, pictureUrl, dateStart, dateEnd, locationName) {
    
    return `
    <div class="item col col-sm-4">
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
              ${dateStart} - ${dateEnd}
          </div>
      </div>
      </div>
    `;
  }

function createPlaceholder(){

  return `
  <div class="card tempCard" aria-hidden="true">
  <img class="card-img-top">
  <div class="card-body">
    <h5 class="card-title placeholder-glow">
      <span class="placeholder col-6"></span>
    </h5>
    <p class="card-text placeholder-glow">
      <span class="placeholder col-7"></span>
      <span class="placeholder col-4"></span>
      <span class="placeholder col-4"></span>
      <span class="placeholder col-6"></span>
      <span class="placeholder col-8"></span>
    </p>
  </div>
  </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    let container = document.querySelector('#masonry-container');

    try{
        const response = await fetch(url);

        if (!response.ok){
            console.error("error")
        } else {
            const data = await response.json();

            
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                
                const column = document.querySelector('.row');
                column.innerHTML += createPlaceholder();


                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {

                  //remove placeholder
                 let placeholderElems = document.getElementsByClassName("tempCard")

                 for(let i = 0; i < placeholderElems.length; i++){
                  placeholderElems[i].parentNode.removeChild(placeholderElems[i]);
                 }

                  const details = await detailResponse.json();
                  const name = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const dateStart = details.conference.starts;
                    let d1 = new Date(dateStart)
                    let newDateStart = (d1.getMonth()+1) + "/" + d1.getDate() + "/" + d1.getFullYear();
                  const dateEnd = details.conference.ends;
                    let d2 = new Date(dateEnd)
                    let newDateEnd = (d2.getMonth()+1) + "/" + d2.getDate() + "/" + d2.getFullYear()
                  const locationName = details.conference.location.name
                  const html = createCard(name, description, pictureUrl, newDateStart, newDateEnd, locationName);
                  const column = document.querySelector('.row');
                  column.innerHTML += html;
                }

                

      }
    }
    } catch (e) {
        console.error("error")
    }

    let msnry = new Masonry( container, {
      itemSelector: '.item',
    });
    $(window).trigger('resize');

    
});
