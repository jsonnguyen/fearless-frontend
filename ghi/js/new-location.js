window.addEventListener('DOMContentLoaded', async () => {

    let url = 'http://localhost:8000/api/states/';
    try{
        let response = await fetch(url);
        
        if (!response.ok){
            console.error('error')
        } else {
            let data = await response.json();
            let stateTag = document.getElementById('state');
            for(let state of data.states){
                let option = document.createElement('option');
                option.value = state.abbreviation;
                option.innerHTML = state.name
                stateTag.appendChild(option);

            }
        }
    } catch (e){
        console.error('error');
    }
    let formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        let formData = new FormData(formTag);
        let json = JSON.stringify(Object.fromEntries(formData));
        let locationUrl = 'http://localhost:8000/api/locations/';
        let fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        let response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            let newLocation = await response.json();
        }
        
    })
});